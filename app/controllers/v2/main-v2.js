import {
  showTTMonAn,
  layTTTuForm,
  onSuccess,
  resetForm,
} from "./controller-v2.js";
const BASE_URL = "https://63f442defe3b595e2ef03bc0.mockapi.io";
let fetchFood = () => {
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then((res) => {
      //   console.log(res.data);
      showTTMonAn(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFood();

let xoaMon = (id) => {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      fetchFood();
      onSuccess("Xóa món thành công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMon = xoaMon;

let createFood = () => {
  let data = layTTTuForm();
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data,
  })
    .then((res) => {
      console.log(res);
      fetchFood();
      onSuccess("Thêm món thành công");
      resetForm();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.createFood = createFood;

let suaMon = (id) => {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      document.getElementById("foodID").value = res.data.id;
      document.getElementById("tenMon").value = res.data.name;
      document.getElementById("loai").value = res.data.type;
      document.getElementById("giaMon").value = res.data.price;
      document.getElementById("khuyenMai").value = res.data.discount;
      document.getElementById("tinhTrang").value = res.data.status;
      document.getElementById("hinhMon").value = res.data.img;
      document.getElementById("moTa").value = res.data.desc;
    })
    .catch((err) => {
      console.log(err);
    });
};
window.suaMon = suaMon;
let capNhat = () => {
  let data = layTTTuForm();
  axios({
    url: `${BASE_URL}/food/${data.id}`,
    method: "PUT",
    data,
  })
    .then((res) => {
      console.log(res);
      fetchFood();
      onSuccess("Cập nhật món thành công");
      resetForm();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.capNhat = capNhat;
