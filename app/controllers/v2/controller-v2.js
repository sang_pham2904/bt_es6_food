export let showTTMonAn = (data) => {
  let contentMonAn = "";
  data.forEach((item) => {
    contentMonAn += `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.type ? "Chay" : "Mặn"}</td>
        <td>${item.price}</td>
        <td>${item.discount}</td>
        <td>0</td>
        <td>${item.status ? "Còn" : "Hết"}</td>
        <td><button class ="btn btn-danger" onclick = "xoaMon(${
          item.id
        })">Xóa</button>
        <button data-toggle="modal"
        data-target="#exampleModal" class ="btn btn-warning" onclick = "suaMon(${
          item.id
        })">Sửa</button>
        </td>
        
        </tr>`;
  });
  document.getElementById("tbodyFood").innerHTML = contentMonAn;
};
export let layTTTuForm = () => {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value;
  let price = document.getElementById("giaMon").value;
  let discount = document.getElementById("khuyenMai").value;
  let status = document.getElementById("tinhTrang").value;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;
  return { id, name, type, price, discount, status, img, desc };
};

export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};

export let resetForm = () => {
  document.getElementById("foodID").value = "";
  document.getElementById("tenMon").value = "";
  document.getElementById("loai").value = "";
  document.getElementById("giaMon").value = "";
  document.getElementById("khuyenMai").value = "";
  document.getElementById("tinhTrang").value = "";
  document.getElementById("hinhMon").value = "";
  document.getElementById("moTa").value = "";
};
